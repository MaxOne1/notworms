﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public TextMeshProUGUI lose;
    private Player playerScript;
    
    public Button reclame;
    public Button restart;
    void Start()
    {
        playerScript = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        lose.gameObject.SetActive(false);
        reclame.gameObject.SetActive(false);
        restart.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(playerScript.isAlive == false)
        {
            lose.gameObject.SetActive(true);
            reclame.gameObject.SetActive(true);
            restart.gameObject.SetActive(true);
        }
        else
        {
            lose.gameObject.SetActive(false);
            reclame.gameObject.SetActive(false);
            restart.gameObject.SetActive(false);
        }      
    }
    public void RestartGame()
    {
        SceneManager.LoadScene(0);
    }


}
