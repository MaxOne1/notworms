﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class Player : MonoBehaviour
{
    public LayerMask clickable;
    public NavMeshAgent myAgent;
   
    private float angleX;
    private float angleY;
    private float sense = 5;

    public int HP;

    private Quaternion originRotate;
    
    private Animator anim;
    
    public GameObject rifle;
    public GameObject pistol;
    
    public bool rifleActive;
    public bool isAlive;
    public bool pistolActive;

    private MoveTime timeScript;

    public GameObject line;



    void Start()
    {
        isAlive = true;
        myAgent = GetComponent<NavMeshAgent>();
        originRotate = transform.rotation;
        anim = GetComponent<Animator>();
        
        rifle.SetActive(false);
        rifleActive = false;

        pistol.SetActive(false);
        pistolActive = false;
        
        timeScript = GameObject.Find("MoveTime").GetComponent<MoveTime>();

        line.SetActive(false);

    }
    void Update()
    {
        Rotate();
        Moving();
        Animation();
        Death();
        WeaponButtons();
    }
    void Rotate()
    {
        if (Input.GetMouseButton(0) && (rifleActive == true || pistolActive == true) && timeScript.playerMove == true )
        {
            angleX += Input.GetAxis("Mouse X") * sense;
            angleY += Input.GetAxis("Mouse Y") * sense;

            Quaternion rotateX = Quaternion.AngleAxis(angleX, Vector3.up);
            Quaternion rotateY = Quaternion.AngleAxis(-angleY, Vector3.right);
            transform.rotation = originRotate * rotateX * rotateY;
            

            line.SetActive(true);
        }
        else
        {
            line.SetActive(false);
        }
    
    }
    void Moving()
    {
        if (Input.GetMouseButtonDown(0) && myAgent.enabled == true && isAlive == true)
        {
            anim.SetBool("Move", true);
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 500, clickable))
            {
                myAgent.SetDestination(hit.point);
            }

        }
        else
        {
            anim.SetBool("Move", false);
        }
    }
    void Animation()
    {
        if (Input.GetKey(KeyCode.Space) && Input.GetMouseButton(0))
        {
            anim.SetBool("Fight", true);
        }
        else
        {
            anim.SetBool("Fight", false);
        }
        
    }
    void Death()
    {
        if (HP <= 0)
        {
            isAlive = false;
            anim.SetBool("Dead", true);
        }
        else
        {
            isAlive = true;
            anim.SetBool("Dead", false);
        }
    }
    void WeaponButtons()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1) && rifleActive == false && timeScript.playerMove == true)
        {
            rifle.SetActive(true);
            rifleActive = true;

            pistol.SetActive(false);
            pistolActive = false;
        }
        if (Input.GetKeyDown(KeyCode.Alpha2) && pistolActive == false && timeScript.playerMove == true)
        {
            pistol.SetActive(true);
            pistolActive = true;

            rifle.SetActive(false);
            rifleActive = false;
            
        }
    }





}
