﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAutogun : MonoBehaviour
{
    public GameObject player;
   
    private Vector3 playerPos;
    
    public Transform muzzle;
    
    public int damage = 3;
    public int bulletCount = 10;

    private float nextFire = 0f;
    public float fireRate;
    
    public AudioClip shoot;
    private AudioSource shootAudio;
    
    private Player playerScript;
    
    private Enemy enemyScript;
    
    private MoveTime timeScript;


    void Start()
    {
        shootAudio = GetComponent<AudioSource>();
       
        playerScript = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
       
        enemyScript = GameObject.FindGameObjectWithTag("Enemy").GetComponent<Enemy>();

        timeScript = GameObject.Find("MoveTime").GetComponent<MoveTime>();
       
    }

    // Update is called once per frame
    void Update()
    {
            if(enemyScript.distance <= 3 && playerScript != null && playerScript.isAlive == true && Time.time > nextFire)
        {
            playerPos = player.transform.position;
            Shoot();
        }
            
    }

    void Shoot()
    {
        if (timeScript.enemyMove == true && enemyScript.distance <= 2 && bulletCount != 0 && enemyScript.isAlive == true)
        {
            Ray ray = new Ray(muzzle.position, playerPos - transform.position + new Vector3(0, 0.5f, 0));
            RaycastHit hit;

            Debug.DrawRay(transform.position, ray.direction * 100, Color.red);
            shootAudio.PlayOneShot(shoot, 1f);
            bulletCount--;
            nextFire = Time.time + 1f / fireRate;
            if (Physics.Raycast(ray, out hit, 100))
            {
                Debug.Log(hit.collider.gameObject.name);
                if (hit.collider.gameObject.tag == "Player")
                {
                    playerScript.HP -= damage;
                }
            }
        }
    }
}
